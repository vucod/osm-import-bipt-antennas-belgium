from math import radians, cos, sin, asin, sqrt
from sklearn.neighbors import KDTree
import geojson
from OSMPythonTools.overpass import Overpass
import pandas as pd
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from typing import List

mean_earth_radius = 6371.0088
overpass = Overpass()


def haversine(lon1: float, lat1: float, lon2: float, lat2: float) -> float:
    """
    Calculate the distance between two points on the earth
    (specified in decimal degrees)
    https://stackoverflow.com/questions/15736995/how-can-i-quickly-estimate-the-distance-between-two-latitude-longitude-points
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    m = mean_earth_radius * 1000 * c
    return m


def getNeighbours(coordinates: List[List[float]],
                  coord_osm: List[List[float]]) -> List[List[float]]:
    """
    Get osm neighbours of coordinates. (lat, lon)
    """
    neighbours = []
    X = pd.DataFrame(coord_osm)
    X.rename(columns={0: "lat", 1: "lon"}, inplace=True)
    for coord in coordinates:
        lat = coord[0]
        lon = coord[1]
        Xa = X.copy()
        Xa.loc[len(X), :] = lat, lon
        sample = Xa.iloc[-1, :].values.reshape(1, -1)
        kdt = KDTree(Xa, leaf_size=30, metric='euclidean')
        res = kdt.query(sample, k=2, return_distance=True)
        neighbour_id = res[1][0][1]
        neighbour_dist = haversine(
            lon, lat,
            X.loc[neighbour_id, "lon"],
            X.loc[neighbour_id, "lat"]
        )
        neighbours.append([neighbour_id, neighbour_dist])
    return neighbours


def closeToElements(lats: List[str], lons: List[str], dist: str) -> List[int]:
    """
    Check if the coordinates are close to elements.
    """
    query = """    """
    for lat, lon in zip(lats, lons):
        query += f"""
        node(around:{dist}, {lat}, {lon});
        out count;
    """
    result = overpass.query(query, timeout=15000)
    return list(map(lambda x: int(x.tags()['total']), result.elements()))


def onBuilding(lats: str, lons: str) -> List[bool]:
    """
    Check if the coordinate is on a building.
    Works only for buildings that have a node
    closer than 25 meters from the coordinate.
    """
    query = """    """
    for lat, lon in zip(lats, lons):
        query += f"""
        way(around:5, {lat}, {lon})[building];
        if(count(ways) < 1) {{
            way(around:10, {lat}, {lon})[building];
          if(count(ways) < 1) {{
               way(around:15, {lat}, {lon})[building];
            if(count(ways) < 1) {{
                  way(around:20, {lat}, {lon})[building];
                 if(count(ways) < 1) {{
                     way(around:25, {lat}, {lon})[building];
                 }}}} }}}}
        out count;
        out geom;
        """
    result = overpass.query(query, timeout=15000)
    elements = result.toJSON()["elements"]

    import json
    with open('result.json', 'w') as fp:
        json.dump(elements, fp)

    list_ = []
    for lat, lon in zip(lats, lons):
        print("lat:", lat,   "---", len(elements))
        point = Point(lat, lon)
        onBuilding = False
        sizeSet = int(elements[0]["tags"]['total'])
        print("## Start new el:", elements[0])
        elements.pop(0)
        for item in range(sizeSet):
            element = elements[item]
            nodes = [(node["lat"], node["lon"])
                     for node in element["geometry"]]
            polygon = Polygon(nodes)
            onBuilding = polygon.contains(point)
            if onBuilding:
                print("--> the coordinate is inside a building")
                break
        del elements[:sizeSet]
        list_.append(onBuilding)
    return list_


def insert_features(row, features, tagsMapping):
    features.append(
            geojson.Feature(
                geometry=geojson.Point((row["lon"], row["lat"])),
                properties=tagsMapping(row)
            )
    )


def df2geojson(df: pd.DataFrame, tagsMapping, name: str):
    """
    df needs to contains the columns:
       - lat
       - lon
       - the columns defined inside tagsMapping
    """
    features = []
    df.apply(lambda row: insert_features(row, features, tagsMapping), axis=1)
    featureCol = geojson.FeatureCollection(features)

    print("# First feature: \n" + geojson.dumps(featureCol[0], indent=2))

    with open(name, 'w') as fp:
        geojson.dump(featureCol, fp, sort_keys=True,
                     ensure_ascii=True, default=str)
