from OSMPythonTools.overpass import Overpass
import pandas as pd


def getAntennas() -> pd.DataFrame():
    """ Get all nodes, ways and relations that could be antennas.
    Limited to Belgium.
    """
    overpass = Overpass()
    result = overpass.query("""

    (
    (area["ISO3166-1"="BE"][admin_level=2];)->.bel;
    nwr["communication:mobile_phone"="yes"](area.bel);
    nwr["telecom"="antenna"](area.bel);
    /*Not always a gsm antenna but safer to take them*/
    nwr["tower:type"="communication"](area.bel);
    );
    out;
    """, timeout=50)
    dict_ = result.toJSON()['elements']
    df = pd.DataFrame(dict_)
    df = df[df.type != "area"]
    return df
