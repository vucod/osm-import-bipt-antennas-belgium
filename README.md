# [OSM import] BIPT Antennas - Belgium

This repository contains the workflow to add antennas from the BIPT into OpenStreetMap

The notebook contains the workflow. The .geojosm file is the output

More information can be found here: https://wiki.openstreetmap.org/wiki/Import/Catalogue/ibpt_belgium_antennas
